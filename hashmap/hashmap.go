package hashmap

import (
	"bytes"
	"errors"
	"fmt"
)

const (
	loadFactor       = 0.75
	blockSizeDefault = 16

	maxInt      = int(^uint(0) >> 1)
	maxCapacity = maxInt
)

var (
	ErrOverflow       = errors.New("Overflow")
	ErrGlobalOverflow = errors.New("Global overflow")
	ErrNotFound       = errors.New("Not found")
)

type Key string

type HashFunc func(blockSize int, k Key) int

type HashMaper interface {
	Set(key Key, value interface{}) error
	Get(key Key) (value interface{}, err error)
	Unset(key Key) error
	Count() int
}

type HashMap struct {
	capacity  int
	storage   []entry
	threshold int
	size      int

	hash HashFunc
}

func NewHashMap(blockSize int, hashFunc HashFunc) HashMaper {
	hashMap := new(HashMap)
	if blockSize != 0 {
		hashMap.capacity = blockSize
	} else {
		hashMap.capacity = blockSizeDefault
	}
	hashMap.storage = make([]entry, hashMap.capacity, hashMap.capacity)
	hashMap.threshold = int(float64(hashMap.capacity) * loadFactor)
	if hashFunc != nil {
		hashMap.hash = hashFunc
	} else {
		hashMap.hash = HashFuncDefault
	}
	return hashMap
}

func (hashMap *HashMap) Set(key Key, value interface{}) (err error) {
	if hashMap.size >= maxInt {
		err = ErrOverflow
		return
	}

	hash := hashMap.hash(hashMap.capacity, key)
	if exist := hashMap.checkEntiesAndSet(hash, key, value); !exist {
		hashMap.size++
		if hashMap.size >= hashMap.threshold {
			err = hashMap.resizeDouble()
			if err != nil {
				return
			}
		}
	}
	return
}

func (hashMap *HashMap) Get(key Key) (value interface{}, err error) {
	hash := hashMap.hash(hashMap.capacity, key)
	value, exist := hashMap.checkEntiesAndGet(hash, key)
	if !exist {
		err = ErrNotFound
		return
	}

	return
}

func (hashMap *HashMap) Unset(key Key) (err error) {
	hash := hashMap.hash(hashMap.capacity, key)
	if !hashMap.checkEntiesAndUnset(hash, key) {
		err = ErrNotFound
		return
	}

	hashMap.size--
	return
}

func (hashMap *HashMap) Count() int {
	return hashMap.size
}

func (hashMap *HashMap) String() string {
	buff := new(bytes.Buffer)
	buff.WriteString("[")
	count := 0
	for i := range hashMap.storage {
		e := &hashMap.storage[i]
		for e != nil && e.notEmpty {
			buff.WriteString(fmt.Sprintf("%v:%v", e.key, e.value))
			if count < hashMap.size-1 {
				buff.WriteString(" ")
			}
			count++
			e = e.next
		}
	}
	buff.WriteString("]")
	return buff.String()
}

func (hashMap *HashMap) Raw() string {
	buff := new(bytes.Buffer)
	buff.WriteString(fmt.Sprintf("HashMap [capacity:%d loadFactor:%f threshold:%d size:%d\n", hashMap.capacity, loadFactor, hashMap.threshold, hashMap.size))
	for i := range hashMap.storage {
		e := &hashMap.storage[i]
		buff.WriteString(fmt.Sprintf(" |-- %d{%p} ---> ", i, e))
		for e != nil && e.notEmpty {
			buff.WriteString(fmt.Sprintf("%p%+v ---> ", e, e))
			e = e.next
		}
		buff.WriteString("nil")
		if i < len(hashMap.storage)-1 {
			buff.WriteString("\n")
		}
	}
	return buff.String()
}

func (hashMap *HashMap) checkEntiesAndGet(hash int, key Key) (value interface{}, exist bool) {
	e := &hashMap.storage[hash]
	for e != nil && e.notEmpty {
		if e.key == key {
			value = e.value
			exist = true
			return
		}

		e = e.next
	}
	return
}

func (hashMap *HashMap) checkEntiesAndSet(hash int, key Key, value interface{}) (exist bool) {

	e := &hashMap.storage[hash]
	for e != nil && e.notEmpty {
		if e.key == key {
			exist = true
			e.value = value
			return
		}

		e = e.next
	}
	en := hashMap.storage[hash]
	hashMap.storage[hash] = entry{
		notEmpty: true,
		key:      key,
		value:    value,
	}
	if en.notEmpty {
		hashMap.storage[hash].next = &entry{
			notEmpty: true,
			key:      en.key,
			value:    en.value,
			next:     en.next,
		}
	}
	return
}

func (hashMap *HashMap) checkEntiesAndUnset(hash int, key Key) (exist bool) {
	e := &hashMap.storage[hash]
	var eLast *entry
	for e != nil && e.notEmpty {
		if e.key == key {
			exist = true
			if eLast == nil {
				if e.next != nil {
					hashMap.storage[hash] = *e.next
				} else {
					hashMap.storage[hash].notEmpty = false
				}
			} else {
				eLast.next = e.next
			}
			return
		}

		eLast = e
		e = e.next
	}
	return
}

func (hashMap *HashMap) resizeDouble() (err error) {
	if hashMap.capacity == maxCapacity {
		err = ErrGlobalOverflow
		return
	} else if (hashMap.capacity << 1) <= maxCapacity {
		hashMap.capacity = hashMap.capacity << 1
		hashMap.threshold = int(float64(hashMap.capacity) * loadFactor)
	} else {
		hashMap.capacity = maxCapacity
		hashMap.threshold = maxCapacity
	}

	storageNew := make([]entry, hashMap.capacity, hashMap.capacity)
	hashMap.transferTo(storageNew)
	hashMap.storage = hashMap.storage[:0]
	hashMap.storage = storageNew
	return
}

func (hashMap *HashMap) transferTo(storageNew []entry) {
	for _, en := range hashMap.storage {
		e := &en
		for e != nil && e.notEmpty {
			hash := hashMap.hash(hashMap.capacity, e.key)
			enNew := storageNew[hash]
			storageNew[hash].notEmpty = true
			storageNew[hash].key = e.key
			storageNew[hash].value = e.value
			if enNew.notEmpty {
				storageNew[hash].next = &entry{
					notEmpty: true,
					key:      enNew.key,
					value:    enNew.value,
					next:     enNew.next,
				}
			}
			e = e.next
		}
	}
}

type entry struct {
	notEmpty bool
	key      Key
	value    interface{}
	next     *entry
}

func HashFuncDefault(blockSize int, k Key) int {
	xxhash64 := xxhash64Checksum([]byte(k), 0)
	hash := int(xxhash64 % uint64(blockSize))
	return hash % blockSize
}

const (
	prime64_1 = 11400714785074694791
	prime64_2 = 14029467366897019727
	prime64_3 = 1609587929392839161
	prime64_4 = 9650029242287828579
	prime64_5 = 2870177450012600261
)

func u64(buf []byte) uint64 {
	// go compiler recognizes this pattern and optimizes it on little endian platforms
	return uint64(buf[0]) | uint64(buf[1])<<8 | uint64(buf[2])<<16 | uint64(buf[3])<<24 | uint64(buf[4])<<32 | uint64(buf[5])<<40 | uint64(buf[6])<<48 | uint64(buf[7])<<56
}

func u32(buf []byte) uint32 {
	return uint32(buf[0]) | uint32(buf[1])<<8 | uint32(buf[2])<<16 | uint32(buf[3])<<24
}

func rol1(u uint64) uint64 {
	return u<<1 | u>>63
}

func rol7(u uint64) uint64 {
	return u<<7 | u>>57
}

func rol11(u uint64) uint64 {
	return u<<11 | u>>53
}

func rol12(u uint64) uint64 {
	return u<<12 | u>>52
}

func rol18(u uint64) uint64 {
	return u<<18 | u>>46
}

func rol23(u uint64) uint64 {
	return u<<23 | u>>41
}

func rol27(u uint64) uint64 {
	return u<<27 | u>>37
}

func rol31(u uint64) uint64 {
	return u<<31 | u>>33
}

func xxhash64Checksum(input []byte, seed uint64) uint64 {
	n := len(input)
	var h64 uint64

	if n >= 32 {
		v1 := seed + prime64_1 + prime64_2
		v2 := seed + prime64_2
		v3 := seed
		v4 := seed - prime64_1
		p := 0
		for n := n - 32; p <= n; p += 32 {
			sub := input[p:][:32] //BCE hint for compiler
			v1 = rol31(v1+u64(sub[:])*prime64_2) * prime64_1
			v2 = rol31(v2+u64(sub[8:])*prime64_2) * prime64_1
			v3 = rol31(v3+u64(sub[16:])*prime64_2) * prime64_1
			v4 = rol31(v4+u64(sub[24:])*prime64_2) * prime64_1
		}

		h64 = rol1(v1) + rol7(v2) + rol12(v3) + rol18(v4)

		v1 *= prime64_2
		v2 *= prime64_2
		v3 *= prime64_2
		v4 *= prime64_2

		h64 = (h64^(rol31(v1)*prime64_1))*prime64_1 + prime64_4
		h64 = (h64^(rol31(v2)*prime64_1))*prime64_1 + prime64_4
		h64 = (h64^(rol31(v3)*prime64_1))*prime64_1 + prime64_4
		h64 = (h64^(rol31(v4)*prime64_1))*prime64_1 + prime64_4

		h64 += uint64(n)

		input = input[p:]
		n -= p
	} else {
		h64 = seed + prime64_5 + uint64(n)
	}

	p := 0
	for n := n - 8; p <= n; p += 8 {
		sub := input[p : p+8]
		h64 ^= rol31(u64(sub)*prime64_2) * prime64_1
		h64 = rol27(h64)*prime64_1 + prime64_4
	}
	if p+4 <= n {
		sub := input[p : p+4]
		h64 ^= uint64(u32(sub)) * prime64_1
		h64 = rol23(h64)*prime64_2 + prime64_3
		p += 4
	}
	for ; p < n; p++ {
		h64 ^= uint64(input[p]) * prime64_5
		h64 = rol11(h64) * prime64_1
	}

	h64 ^= h64 >> 33
	h64 *= prime64_2
	h64 ^= h64 >> 29
	h64 *= prime64_3
	h64 ^= h64 >> 32

	return h64
}
