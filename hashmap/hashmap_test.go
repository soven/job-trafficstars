package hashmap

import (
	"math/rand"
	"testing"
)

const (
	alphabetSet       = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
	seed              = 40
	testHashMapPairsN = 5
)

// len [3, 7]
func randKey() string {
	key := ""
	keyLen := rand.Intn(5) + 3
	for i := 0; i < keyLen; i++ {
		key += string(alphabetSet[rand.Intn(len(alphabetSet))])
	}
	return key
}

func genTestPairs(n int) (pairs []struct {
	key   Key
	value string
}) {
	pairs = make([]struct {
		key   Key
		value string
	}, n, n)
	for i := 0; i < n; i++ {
		pairs[i].key = Key(randKey())
		pairs[i].value = randKey()
	}
	return
}

func genUniqTestPairs(n int) (pairs []struct {
	key   Key
	value string
}) {
	pairsMap := make(map[Key]bool)
	pairs = make([]struct {
		key   Key
		value string
	}, n, n)
	const iterCountThreshold = 100
	for i := 0; i < n; i++ {
		iterCount := 0
		key := Key(randKey())
		for pairsMap[key] {
			if iterCount >= iterCountThreshold {
				panic("Something went wrong")
			}
			key = Key(randKey())
		}
		pairsMap[key] = true
		pairs[i].key = key
		pairs[i].value = randKey()
	}
	return
}

func testHashMap(blockSize int, pairs []struct {
	key   Key
	value string
}, t *testing.T) {
	hashMap := NewHashMap(blockSize, nil)

	for _, p := range pairs {
		if err := hashMap.Set(p.key, p.value); (err != nil) != false {
			t.Errorf("HashMap.Set(p.key, p.value); error = %v, wantErr %v", err, false)
		}
	}
	if count := hashMap.Count(); count != len(pairs) {
		t.Errorf("HashMap.Count() When len(pairs); count = %v, len(pairs) %v", count, len(pairs))
	}
	for _, p := range pairs {
		if value, err := hashMap.Get(p.key); (err != nil) != false {
			t.Errorf("HashMap.Get(p.key); error = %v, wantErr %v", err, false)
		} else if value != p.value {
			t.Errorf("HashMap.Get(p.key); value = %v, p.value %v", value, p.value)
		}
	}
	keyNotFound := Key("ab")
	if _, err := hashMap.Get(keyNotFound); (err != nil) != true {
		t.Errorf("HashMap.Get(keyNotFound); error = %v, wantErr %v", err, true)
	}
	keyLastPair := pairs[len(pairs)-1].key
	valueNewForLastPair := "ef"
	if err := hashMap.Set(keyLastPair, valueNewForLastPair); (err != nil) != false {
		t.Errorf("HashMap.Set(keyLastPair, valueNewForLastPair); error = %v, wantErr %v", err, false)
	}
	if count := hashMap.Count(); count != len(pairs) {
		t.Errorf("HashMap.Count() When len(pairs); count = %v, len(pairs) %v", count, len(pairs))
	}
	if value, err := hashMap.Get(keyLastPair); (err != nil) != false {
		t.Errorf("HashMap.Get(keyLastPair); error = %v, wantErr %v", err, false)
	} else if value != valueNewForLastPair {
		t.Errorf("HashMap.Get(keyLastPair); value = %v, valueNewForLastPair %v", value, valueNewForLastPair)
	}
	keyNew := Key("gh")
	valueNew := "ij"
	if _, err := hashMap.Get(keyNew); (err != nil) != true {
		t.Errorf("HashMap.Get(keyNew) When not exist; error = %v, wantErr %v", err, true)
	}
	if err := hashMap.Set(keyNew, valueNew); (err != nil) != false {
		t.Errorf("HashMap.Set(keyNew, valueNew); error = %v, wantErr %v", err, false)
	}
	if count := hashMap.Count(); count != len(pairs)+1 {
		t.Errorf("HashMap.Count() When len(pairs)+1; count = %v, len(pairs) + 1 %v", count, len(pairs)+1)
	}
	if value, err := hashMap.Get(keyNew); (err != nil) != false {
		t.Errorf("HashMap.Get(keyNew); error = %v, wantErr %v", err, false)
	} else if value != valueNew {
		t.Errorf("HashMap.Get(keyNew); value = %v, valueNew %v", value, valueNew)
	}
	for _, p := range pairs {
		if err := hashMap.Unset(p.key); (err != nil) != false {
			t.Errorf("HashMap.Unset(p.key); error = %v, wantErr %v", err, false)
		}
	}
	if count := hashMap.Count(); count != 1 {
		t.Errorf("HashMap.Count() When 1; count = %v", count)
	}
	if err := hashMap.Unset(keyNew); (err != nil) != false {
		t.Errorf("HashMap.Unset(keyNew); error = %v, wantErr %v", err, false)
	}
	if count := hashMap.Count(); count != 0 {
		t.Errorf("HashMap.Count() When 0; count = %v", count)
	}
	if err := hashMap.Unset(keyNew); (err != nil) != true {
		t.Errorf("HashMap.Unset(keyNew) When not exist; error = %v, wantErr %v", err, true)
	}
}

func TestHashMap(t *testing.T) {
	rand.Seed(seed)
	pairs := genUniqTestPairs(testHashMapPairsN)
	tests := []struct {
		name      string
		blockSize int
	}{
		{"BlockSize_16", 16},
		{"BlockSize_64", 64},
		{"BlockSize_128", 128},
		{"BlockSize_1024", 1024},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			testHashMap(tt.blockSize, pairs, t)
		})
	}
}

func testHashFunc(blockSize int, pairs []struct {
	key   Key
	value string
}, t *testing.T) {
	collisionMap := make(map[int]bool)
	collisionCount := 0
	collisionCountForLoadFactor := 0
	numForLoadFactor := int(loadFactor * float64(len(pairs)))
	for i, p := range pairs {
		hash := HashFuncDefault(blockSize, p.key)
		if collisionMap[hash] {
			collisionCount++
			if i < numForLoadFactor {
				collisionCountForLoadFactor++
			}
		} else {
			collisionMap[hash] = true
		}
	}
	collisionPercentForLoadFactor := float64(collisionCountForLoadFactor) * 100.0 / float64(numForLoadFactor)
	t.Logf("Collision percent for loadFactor %d keys: %.2f%%", numForLoadFactor, collisionPercentForLoadFactor)
	collisionPercentCurrent := float64(collisionCount) * 100.0 / float64(len(pairs))
	t.Logf("Collision percent for %d keys: %.2f%%", len(pairs), collisionPercentCurrent)
}

func TestHashFunc(t *testing.T) {
	rand.Seed(seed)
	tests := []struct {
		name      string
		blockSize int
	}{
		{"BlockSize_16", 16},
		{"BlockSize_64", 64},
		{"BlockSize_128", 128},
		{"BlockSize_1024", 1024},
	}
	for _, tt := range tests {
		pairs := genUniqTestPairs(tt.blockSize)
		t.Run(tt.name, func(t *testing.T) {
			testHashFunc(tt.blockSize, pairs, t)
		})
	}
}

// Set

func benchmarkHashMap_Set(blockSize int, b *testing.B) {
	hashMap := NewHashMap(blockSize, nil)
	for i := 0; i < b.N; i++ {
		b.StopTimer()
		pairs := genTestPairs(1)
		key := pairs[0].key
		value := pairs[0].value
		b.StartTimer()
		hashMap.Set(key, value)
	}
}

func BenchmarkHashMap_Set(b *testing.B) {
	rand.Seed(seed)
	benchmarks := []struct {
		name      string
		blockSize int
	}{
		{"BlockSize_16", 16},
		{"BlockSize_64", 64},
		{"BlockSize_128", 128},
		{"BlockSize_1024", 1024},
	}
	for _, bb := range benchmarks {
		b.Run(bb.name, func(b *testing.B) {
			benchmarkHashMap_Set(bb.blockSize, b)
		})
	}
}

func benchmarkNativeMap_Set(blockSize int, b *testing.B) {
	hashMap := make(map[Key]interface{}, blockSize)
	for i := 0; i < b.N; i++ {
		b.StopTimer()
		pairs := genTestPairs(1)
		key := pairs[0].key
		value := pairs[0].value
		b.StartTimer()
		hashMap[key] = value
	}
}

func BenchmarkNativeMap_Set(b *testing.B) {
	rand.Seed(seed)
	benchmarks := []struct {
		name      string
		blockSize int
	}{
		{"BlockSize_16", 16},
		{"BlockSize_64", 64},
		{"BlockSize_128", 128},
		{"BlockSize_1024", 1024},
	}
	for _, bb := range benchmarks {
		b.Run(bb.name, func(b *testing.B) {
			benchmarkNativeMap_Set(bb.blockSize, b)
		})
	}
}

// Get

func benchmarkHashMap_Get(blockSize int, b *testing.B) {
	hashMap := NewHashMap(blockSize, nil)
	for i := 0; i < b.N; i++ {
		b.StopTimer()
		pairs := genTestPairs(1)
		key := pairs[0].key
		b.StartTimer()
		_, _ = hashMap.Get(key)
	}
}

func BenchmarkHashMap_Get(b *testing.B) {
	rand.Seed(seed)
	benchmarks := []struct {
		name      string
		blockSize int
	}{
		{"BlockSize_16", 16},
		{"BlockSize_64", 64},
		{"BlockSize_128", 128},
		{"BlockSize_1024", 1024},
	}
	for _, bb := range benchmarks {
		b.Run(bb.name, func(b *testing.B) {
			benchmarkHashMap_Get(bb.blockSize, b)
		})
	}
}

func benchmarkNativeMap_Get(blockSize int, b *testing.B) {
	hashMap := make(map[Key]interface{}, blockSize)
	for i := 0; i < b.N; i++ {
		b.StopTimer()
		pairs := genTestPairs(1)
		key := pairs[0].key
		b.StartTimer()
		_ = hashMap[key]
	}
}

func BenchmarkNativeMap_Get(b *testing.B) {
	rand.Seed(seed)
	benchmarks := []struct {
		name      string
		blockSize int
	}{
		{"BlockSize_16", 16},
		{"BlockSize_64", 64},
		{"BlockSize_128", 128},
		{"BlockSize_1024", 1024},
	}
	for _, bb := range benchmarks {
		b.Run(bb.name, func(b *testing.B) {
			benchmarkNativeMap_Get(bb.blockSize, b)
		})
	}
}

// Unset

func benchmarkHashMap_Unset(blockSize int, b *testing.B) {
	hashMap := NewHashMap(blockSize, nil)
	for i := 0; i < b.N; i++ {
		b.StopTimer()
		pairs := genTestPairs(1)
		key := pairs[0].key
		b.StartTimer()
		_ = hashMap.Unset(key)
	}
}

func BenchmarkHashMap_Unset(b *testing.B) {
	rand.Seed(seed)
	benchmarks := []struct {
		name      string
		blockSize int
	}{
		{"BlockSize_16", 16},
		{"BlockSize_64", 64},
		{"BlockSize_128", 128},
		{"BlockSize_1024", 1024},
	}
	for _, bb := range benchmarks {
		b.Run(bb.name, func(b *testing.B) {
			benchmarkHashMap_Unset(bb.blockSize, b)
		})
	}
}

func benchmarkNativeMap_Unset(blockSize int, b *testing.B) {
	hashMap := make(map[Key]interface{}, blockSize)
	for i := 0; i < b.N; i++ {
		b.StopTimer()
		pairs := genTestPairs(1)
		key := pairs[0].key
		b.StartTimer()
		delete(hashMap, key)
	}
}

func BenchmarkNativeMap_Unset(b *testing.B) {
	rand.Seed(seed)
	benchmarks := []struct {
		name      string
		blockSize int
	}{
		{"BlockSize_16", 16},
		{"BlockSize_64", 64},
		{"BlockSize_128", 128},
		{"BlockSize_1024", 1024},
	}
	for _, bb := range benchmarks {
		b.Run(bb.name, func(b *testing.B) {
			benchmarkNativeMap_Unset(bb.blockSize, b)
		})
	}
}

// HashFunc

func benchmarkHashFunc(blockSize int, b *testing.B) {
	for i := 0; i < b.N; i++ {
		b.StopTimer()
		pairs := genTestPairs(1)
		key := pairs[0].key
		b.StartTimer()
		_ = HashFuncDefault(blockSize, key)
	}
}

func BenchmarkHashFunc(b *testing.B) {
	rand.Seed(seed)
	benchmarks := []struct {
		name      string
		blockSize int
	}{
		{"BlockSize_16", 16},
		{"BlockSize_64", 64},
		{"BlockSize_128", 128},
		{"BlockSize_1024", 1024},
	}
	for _, bb := range benchmarks {
		b.Run(bb.name, func(b *testing.B) {
			benchmarkHashFunc(bb.blockSize, b)
		})
	}
}
