## Test for "HashMap"

```
$ go test -run=^TestHashMap -v
=== RUN   TestHashMap
=== RUN   TestHashMap/BlockSize_16
=== RUN   TestHashMap/BlockSize_64
=== RUN   TestHashMap/BlockSize_128
=== RUN   TestHashMap/BlockSize_1024
--- PASS: TestHashMap (0.00s)
    --- PASS: TestHashMap/BlockSize_16 (0.00s)
    --- PASS: TestHashMap/BlockSize_64 (0.00s)
    --- PASS: TestHashMap/BlockSize_128 (0.00s)
    --- PASS: TestHashMap/BlockSize_1024 (0.00s)
PASS

```


```
$ go test -run=^TestHashFunc -v
=== RUN   TestHashFunc
=== RUN   TestHashFunc/BlockSize_16
=== RUN   TestHashFunc/BlockSize_64
=== RUN   TestHashFunc/BlockSize_128
=== RUN   TestHashFunc/BlockSize_1024
--- PASS: TestHashFunc (0.00s)
    --- PASS: TestHashFunc/BlockSize_16 (0.00s)
    	hashmap_test.go:177: Collision percent for loadFactor 12 keys: 33.33%
    	hashmap_test.go:179: Collision percent for 16 keys: 37.50%
    --- PASS: TestHashFunc/BlockSize_64 (0.00s)
    	hashmap_test.go:177: Collision percent for loadFactor 48 keys: 18.75%
    	hashmap_test.go:179: Collision percent for 64 keys: 31.25%
    --- PASS: TestHashFunc/BlockSize_128 (0.00s)
    	hashmap_test.go:177: Collision percent for loadFactor 96 keys: 36.46%
    	hashmap_test.go:179: Collision percent for 128 keys: 41.41%
    --- PASS: TestHashFunc/BlockSize_1024 (0.00s)
    	hashmap_test.go:177: Collision percent for loadFactor 768 keys: 28.78%
    	hashmap_test.go:179: Collision percent for 1024 keys: 35.64%
PASS
```

```
$ go test -bench=^Benchmark.*_Set -benchmem -benchtime=200ms -run=No
goos: linux
goarch: amd64
BenchmarkHashMap_Set/BlockSize_16-4         	  500000	      1096 ns/op	     248 B/op	       1 allocs/op
BenchmarkHashMap_Set/BlockSize_64-4         	  500000	      1069 ns/op	     248 B/op	       1 allocs/op
BenchmarkHashMap_Set/BlockSize_128-4        	  500000	      1079 ns/op	     248 B/op	       1 allocs/op
BenchmarkHashMap_Set/BlockSize_1024-4       	  500000	      1069 ns/op	     248 B/op	       1 allocs/op
BenchmarkNativeMap_Set/BlockSize_16-4       	  500000	       694 ns/op	     178 B/op	       1 allocs/op
BenchmarkNativeMap_Set/BlockSize_64-4       	  500000	       806 ns/op	     178 B/op	       1 allocs/op
BenchmarkNativeMap_Set/BlockSize_128-4      	  500000	       782 ns/op	     178 B/op	       1 allocs/op
BenchmarkNativeMap_Set/BlockSize_1024-4     	  500000	       691 ns/op	     178 B/op	       1 allocs/op
PASS
```

```
$ go test -bench=^Benchmark.*_Get -benchmem -benchtime=200ms -run=No
goos: linux
goarch: amd64
BenchmarkHashMap_Get/BlockSize_16-4         	 2000000	       152 ns/op	       0 B/op	       0 allocs/op
BenchmarkHashMap_Get/BlockSize_64-4         	 2000000	       152 ns/op	       0 B/op	       0 allocs/op
BenchmarkHashMap_Get/BlockSize_128-4        	 2000000	       148 ns/op	       0 B/op	       0 allocs/op
BenchmarkHashMap_Get/BlockSize_1024-4       	 2000000	       165 ns/op	       0 B/op	       0 allocs/op
BenchmarkNativeMap_Get/BlockSize_16-4       	 5000000	        75.3 ns/op	       0 B/op	       0 allocs/op
BenchmarkNativeMap_Get/BlockSize_64-4       	 5000000	        78.9 ns/op	       0 B/op	       0 allocs/op
BenchmarkNativeMap_Get/BlockSize_128-4      	 5000000	        78.4 ns/op	       0 B/op	       0 allocs/op
BenchmarkNativeMap_Get/BlockSize_1024-4     	 5000000	        80.9 ns/op	       0 B/op	       0 allocs/op
PASS
```

```
$ go test -bench=^Benchmark.*_Unset -benchmem -benchtime=200ms -run=No
goos: linux
goarch: amd64
BenchmarkHashMap_Unset/BlockSize_16-4         	 2000000	       149 ns/op	       0 B/op	       0 allocs/op
BenchmarkHashMap_Unset/BlockSize_64-4         	 2000000	       160 ns/op	       0 B/op	       0 allocs/op
BenchmarkHashMap_Unset/BlockSize_128-4        	 2000000	       164 ns/op	       0 B/op	       0 allocs/op
BenchmarkHashMap_Unset/BlockSize_1024-4       	 2000000	       233 ns/op	       0 B/op	       0 allocs/op
BenchmarkNativeMap_Unset/BlockSize_16-4       	 3000000	        84.8 ns/op	       0 B/op	       0 allocs/op
BenchmarkNativeMap_Unset/BlockSize_64-4       	 5000000	        86.9 ns/op	       0 B/op	       0 allocs/op
BenchmarkNativeMap_Unset/BlockSize_128-4      	 3000000	        86.9 ns/op	       0 B/op	       0 allocs/op
BenchmarkNativeMap_Unset/BlockSize_1024-4     	 3000000	        84.0 ns/op	       0 B/op	       0 allocs/op
PASS
```

```
$ go test -bench=^BenchmarkHashFunc -benchmem -benchtime=200ms -run=No
goos: linux
goarch: amd64
BenchmarkHashFunc/BlockSize_16-4         	 2000000	       130 ns/op	       0 B/op	       0 allocs/op
BenchmarkHashFunc/BlockSize_64-4         	 2000000	       122 ns/op	       0 B/op	       0 allocs/op
BenchmarkHashFunc/BlockSize_128-4        	 2000000	       121 ns/op	       0 B/op	       0 allocs/op
BenchmarkHashFunc/BlockSize_1024-4       	 3000000	       120 ns/op	       0 B/op	       0 allocs/op
PASS
```


## Test for "BidService"

```
$ go test -run=TestWinner_RandomSets
PASS
```

```
$ go test -v -run=TestWinner_RandomSets_Timing
=== RUN   TestWinner_RandomSets_Timing
--- PASS: TestWinner_RandomSets_Timing (7.57s)
	bidservice_test.go:175: tooLongCountPercent: 1.00%
PASS
```

```
$ go test -bench=^BenchmarkWinner -benchtime=10s -run=No
goos: linux
goarch: amd64
BenchmarkWinner_RandomSets-4   	     200	  80018410 ns/op
PASS
```
