package main

import (
	"context"
	"encoding/json"
	"flag"
	"log"
	"net/http"
	"net/url"
	"time"
)

const (
	CLIENT_TIMEOUT_MS = 95 // < 100
)

type result struct {
	Price  int    `json:"price"`
	Source string `json:"source"`
}

type row struct {
	Price int `json:"price"`
}

func main() {
	listenAddr := flag.String("addr", ":8888", "http listen address")
	flag.Parse()

	http.HandleFunc("/winner", winner())

	log.Printf("Listen on %s", *listenAddr)
	log.Fatal(http.ListenAndServe(*listenAddr, nil))
}

func winner() func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		t := time.Now()

		sources := r.URL.Query()["s"]
		if len(sources) == 0 {
			http.Error(w, "empty sources list", http.StatusBadRequest)
			return
		}
		dprintf("sources: %+v", sources)

		res, err := defineWinner(sources)
		if err != nil {
			http.Error(w, "Something went wrong", http.StatusInternalServerError)
			return
		}

		if res == nil {
			http.Error(w, "all extended sources unavailable", http.StatusBadRequest)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)

		json.NewEncoder(w).Encode(res)

		tmprint("Full", t)
	}
}

func defineWinner(sources []string) (res *result, err error) {

	ctx, cancel := context.WithTimeout(context.Background(), CLIENT_TIMEOUT_MS*time.Millisecond)
	resChan := make(chan *result)

	for _, source := range filterUniq(sources) {

		go func(source string) {

			req, err := http.NewRequest(http.MethodGet, source, nil)
			if err != nil {
				return
			}
			req = req.WithContext(ctx)

			t0 := time.Now()
			resp, err := http.DefaultClient.Do(req)
			tmprint("Get", t0)
			if err != nil {
				uerr, ok := err.(*url.Error)
				isTimeout := ok && uerr.Timeout()
				dprintf("isTimeout: %+v", isTimeout)
				if isTimeout {
					return
				} else {
					dprintf("err: %+v", err)
				}
				return
			}

			if resp.StatusCode != http.StatusOK {
				dprintf("status: %+v", resp.Status)
				return
			}

			rows := make([]row, 0, 0)
			err = json.NewDecoder(resp.Body).Decode(&rows)
			if err != nil {
				dprintf("err: %+v", err)
				return
			}

			resp.Body.Close()

			dprintf("rows: %+v", rows)

			if len(rows) == 0 {
				return
			}

			resChan <- &result{
				Source: source,
				Price:  getMax(rows),
			}

		}(source)

	}

LISTEN:
	for {
		select {
		case resSource := <-resChan:
			if res == nil || resSource.Price >= res.Price {
				res = resSource
			}
		case <-ctx.Done():
			dprintln("done")
			cancel()
			break LISTEN
		}
	}

	dprintf("max: %+v", res)

	return
}

func filterUniq(sources []string) (sourcesUniq []string) {
	sourcesMap := make(map[string]bool)
	for _, s := range sources {
		if sourcesMap[s] {
			continue
		}
		sourcesMap[s] = true
	}
	for s := range sourcesMap {
		sourcesUniq = append(sourcesUniq, s)
	}
	return
}

func getMax(rows []row) int {
	max := 0
	for _, r := range rows {
		if r.Price > max {
			max = r.Price
		}
	}
	return max
}
