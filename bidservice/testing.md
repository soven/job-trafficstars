```
$ go test -run=TestWinner_RandomSets
PASS
```

```
$ go test -v -run=TestWinner_RandomSets_Timing
=== RUN   TestWinner_RandomSets_Timing
--- PASS: TestWinner_RandomSets_Timing (7.57s)
	bidservice_test.go:175: tooLongCountPercent: 1.00%
PASS
```

```
$ go test -bench=^BenchmarkWinner -benchtime=10s -run=No
goos: linux
goarch: amd64
BenchmarkWinner_RandomSets-4   	     200	  80018410 ns/op
PASS
```
