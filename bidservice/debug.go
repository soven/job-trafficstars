package main

import (
	"log"
	"time"
)

const (
	DEBUG = true
)

func errfatalln(err error, args ...interface{}) {
	if err != nil {
		log.Fatalln(args...)
	}
}

func errfatalf(err error, fmt string, args ...interface{}) {
	if err != nil {
		log.Fatalf(fmt, args...)
	}
}

func dprintln(args ...interface{}) {
	if DEBUG {
		args = append([]interface{}{"[D]"}, args...)
		log.Println(args...)
	}
}
func dprintf(fmt string, args ...interface{}) {
	if DEBUG {
		fmt = "[D] " + fmt
		log.Printf(fmt, args...)
	}
}

func tm(t0 time.Time) time.Duration {
	return time.Now().Sub(t0)
}

func tmprint(prefix string, t0 time.Time) {
	dprintf("[TM] %s: %v", prefix, tm(t0).String())
}
