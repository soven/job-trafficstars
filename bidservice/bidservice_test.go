package main

import (
	"encoding/json"
	"io/ioutil"
	"math/rand"
	"net/http"
	"net/url"
	"strings"
	"testing"
	"time"
)

// It is need to run the bidservice on the addr before the testing

const (
	seed = 80

	setNum = 100

	addr             = "127.0.0.1:8888"
	addrEmptySources = "http://127.0.0.1:8080/empty"

	urlPath     = "winner"
	urlQueryKey = "s"
)

var (
	addrSourcesMax = map[string]int{
		"http://127.0.0.1:8080/primes": 23,
		"http://127.0.0.1:8080/fibo":   21,
		"http://127.0.0.1:8080/fact":   24,
		"http://127.0.0.1:8080/rand":   76,
		addrEmptySources:               -1,
	}
)

func genURL(sources []string) string {
	query := url.Values{}
	for _, s := range sources {
		query.Add(urlQueryKey, s)
	}
	u := url.URL{
		Scheme:   "http",
		Host:     addr,
		Path:     urlPath,
		RawQuery: query.Encode(),
	}
	return u.String()
}

func getRandSourceSet(n int) []string {
	addrSourcesMaxList := make([]string, 0, len(addrSourcesMax))
	for s := range addrSourcesMax {
		addrSourcesMaxList = append(addrSourcesMaxList, s)
	}

	sources := make([]string, 0, n)
	count := 0
	for {
		if count >= n {
			break
		}

		i := rand.Intn(len(addrSourcesMax))
		sources = append(sources, addrSourcesMaxList[i])
		count++
	}
	return sources
}

func TestWinner_RandomSets(t *testing.T) {
	rand.Seed(seed)

	for i := 0; i < setNum; i++ {
		sourceNum := rand.Intn(len(addrSourcesMax) + 1)
		sources := getRandSourceSet(sourceNum)
		t.Logf("Source set: %+v", sources)

		u := genURL(sources)
		t.Logf("URL: %s", u)

		resp, err := http.Get(u)
		if err != nil {
			t.Errorf("Http Get error: %v", err)
			return
		}

		if resp.StatusCode != http.StatusOK {
			resBytes, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				t.Errorf("Errors read response bytes: %v", err)
				return
			}
			resp.Body.Close()

			res := string(resBytes)

			switch resp.StatusCode {
			case http.StatusBadRequest:
				if len(sources) == 0 && strings.HasPrefix(res, "empty sources list") {
					t.Logf("res: %+v", res)
					// OK. When empty sources
					continue
				} else if strings.HasPrefix(res, "all extended sources unavailable") {
					t.Logf("res: %+v", res)
					// OK. When all answer as unavailable or it is timeout reached
					continue
				} else {
					t.Errorf("Unexpected bad request status: %s", res)
					return
				}
			case http.StatusInternalServerError:
				if strings.HasPrefix(res, "Something went wrong") {
					// Unexpected known error
					t.Error("Unexpected known error")
					return
				} else {
					t.Errorf("Unexpected interanl server error status: %s", res)
					return
				}
			default:
				t.Errorf("Unexpected status code %d with status: %s", resp.StatusCode, res)
				return
			}
		}

		res := new(result)
		err = json.NewDecoder(resp.Body).Decode(res)
		if err != nil {
			t.Errorf("Error on json decode result: %v", err)
			return
		}

		resp.Body.Close()

		t.Logf("res: %+v", res)

		if sourcesMaxPrice, exist := addrSourcesMax[res.Source]; !exist {
			t.Errorf("Unexpected source in response: %+v", res)
			return
		} else {
			if res.Price != sourcesMaxPrice {
				t.Errorf("Unexpected max price for source in response: %+v", res)
				return
			}
		}

	}
}

func TestWinner_RandomSets_Timing(t *testing.T) {
	rand.Seed(seed)

	tooLongCount := 0
	for i := 0; i < setNum; i++ {
		sourceNum := rand.Intn(len(addrSourcesMax) + 1)
		sources := getRandSourceSet(sourceNum)

		u := genURL(sources)

		tStart := time.Now()
		_, err := http.Get(u)
		tReq := time.Since(tStart)
		if err != nil {
			t.Errorf("Http Get error: %v", err)
			return
		}

		if tReq > 100*time.Millisecond {
			tooLongCount++
		}

	}
	t.Logf("tooLongCountPercent: %.2f%%", float64(tooLongCount)*100.0/float64(setNum))
}

func BenchmarkWinner_RandomSets(b *testing.B) {
	rand.Seed(seed)
	for i := 0; i < b.N; i++ {
		b.StopTimer()
		sourceNum := rand.Intn(len(addrSourcesMax) + 1)
		sources := getRandSourceSet(sourceNum)

		u := genURL(sources)

		b.StartTimer()
		http.Get(u)
	}
}
